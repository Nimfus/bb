<?php

namespace app\models;
use Yii;


class ContactusForm extends \yii\base\Model
{

    public $username;
    public $email;
    public $message;

    public function formName()
    {
        return 'Contactus';
    }

    public function rules()
    {
        return[
            // name, email and message are required
            [['name', 'email', 'message'], 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            // email has to be a valid email address
            ['email', 'email'],

        ];
    }

    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject = 'Contact')
                ->setTextBody($this->message)
                ->send();

            return true;
        }
        return false;
    }
}