<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacancy".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $friendly
 * @property integer $office
 * @property integer $compensation
 * @property integer $commission
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'friendly', 'office', 'compensation', 'commission'], 'required'],
            [['description'], 'string'],
            [['friendly', 'office', 'compensation', 'commission'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'friendly' => 'Friendly',
            'office' => 'Office',
            'compensation' => 'Compensation',
            'commission' => 'Commission',
        ];
    }
}
