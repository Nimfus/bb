<?php

namespace app\models;

use yii\base\Model;
use Yii;

class LoginForm extends Model
{
	public $username;
	public $password;

	private $_user = false;

	public function rules()
	{
		return [
			[
				['username','password'],
				'required'
			],
			['password','validatePassword'],

		];
	}

    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'name' => 'Name',
            'population' => 'Population',
        ];
    }
	public function validatePassword($attribute)
	{
		if (!$this->hasErrors()):
            $user = $this->getUser();
            if (
                !$user ||
                !$user->validatePassword($this->password)):
                $this->addError($attribute, 'Wrong username or password.');
            endif;
        endif;
	}

	
    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}