<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promotion".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 */
class Promotion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['title', 'content'], 'string'],
            [['button'], 'integer']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'button' => 'Request a demo button',
        ];
    }
}
