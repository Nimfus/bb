<?php
$title = $_POST['title'];

$servername = "127.0.0.1";
$username = "nimfus";
$password = "";
$dbname = "yii2basic";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM vacancy WHERE title = '$title'");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $k=>$v) {
        $perks = ['friendly'=>$v['friendly'], 'office'=>$v['office'], 'compensation' => $v['compensation'], 'commission'=>$v['commission']];
        if ($perks['office']!=0) echo "<div class=\"col-md-4 perk\"><img src=\"../web/images/office.png\"><h5>Great office location</h5></div>";
        if ($perks['friendly']!=0) echo "<div class=\"col-md-4 perk\"><img src=\"../web/images/friendly.png\"><h5>Friendly Team</h5></div>";
        if ($perks['compensation']!=0) echo "<div class=\"col-md-4 perk\"><img src=\"../web/images/compensation.png\"><h5>Fair compensation</h5></div>";
        if ($perks['commission']!=0) echo "<div class=\"col-md-4 perk blank\"></div><div class=\"col-md-4 perk\"><img src=\"../web/images/comission.png\"><h5>Highest Commission rate in industry</h5></div>";
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;