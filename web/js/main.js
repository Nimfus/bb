$(document).ready(function(){
	// Hovering of portfolio items
	$('#products').mouseenter(function(){
		$('#products').attr("src","../web/images/menu-products-grey.png")
	})
	$('#products').mouseleave(function(){
		$('#products').attr("src","../web/images/menu-products.png")
	})

	$('#designs').mouseenter(function(){
		$('#designs').attr("src","../web/images/menu-designs-grey.png")
	})
	$('#designs').mouseleave(function(){
		$('#designs').attr("src","../web/images/menu-designs.png")
	})

	$('#websites').mouseenter(function(){
		$('#websites').attr("src","../web/images/menu-websites-grey.png")
	})
	$('#websites').mouseleave(function(){
		$('#websites').attr("src","../web/images/menu-websites.png")
	})

	$('#other').mouseenter(function(){
		$('#other').attr("src","../web/images/menu-other-grey.png")
	})
	$('#other').mouseleave(function(){
		$('#other').attr("src","../web/images/menu-other.png")
	})
	//------------------------------------------------------------
	// Solutions
	$('.creation').mouseenter(function(){
		$('.creation > p').hide();
		$('.creation').css('background-image',"url('../web/images/creation-bg-green.png')").css('padding-top','40px');
		$('.creation').prepend('<p class="reaction-header">WEBSITE CREATION</p>').append('<ul class="reaction-list"><li>Customised website</li><li>E-commerce</li><li>Content management system and more</li></ul>');

	})
	$('.creation').mouseleave(function(){
		$('.creation > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.creation').css('background-image',"url('../web/images/creation-bg.png')").css('padding-top','140px');
	})

	$('.mobile').mouseenter(function(){
		$('.mobile > p').hide();
		$('.mobile').css('background-image',"url('../web/images/mobile-bg-green.png')").css('padding-top','40px');
		$('.mobile').prepend('<p class="reaction-header">MOBILE APPS</p>').append('<ul class="reaction-list"><li>iOS (Apple) application</li><li>Android application</li><li>App interface design and more</li></ul>');
	})
	$('.mobile').mouseleave(function(){
		$('.mobile > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.mobile').css('background-image',"url('../web/images/mobile-bg.png')").css('padding-top','140px');
	})

	$('.customer').mouseenter(function(){
		$('.customer > p').hide();
		$('.customer').css('background-image',"url('../web/images/customer-bg-green.png')").css('padding-top','40px');
		$('.customer').prepend('<p class="reaction-header">CUSTOMER SOLUTIONS</p>').append('<ul class="reaction-list"><li>Online shopping cart</li><li>Booking system</li><li>Membership system</li><li>Customer management</li><li>DNC and PDPA compliance solutions and more</li></ul>');;
	})
	$('.customer').mouseleave(function(){
		$('.customer > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.customer').css('background-image',"url('../web/images/customer-bg.png')").css('padding-top','140px');
	})

	$('.employee').mouseenter(function(){
		$('.employee > p').hide();
		$('.employee').css('background-image',"url('../web/images/employee-bg-green.png')").css('padding-top','40px');
		$('.employee').prepend('<p class="reaction-header">EMPLOYEE MANAGEMENT</p>').append('<ul class="reaction-list"><li>Online leave management</li><li>Automatic Payroll management</li><li>Automatic claim management and more</li></ul>');
	})
	$('.employee').mouseleave(function(){
		$('.employee > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.employee').css('background-image',"url('../web/images/employee-bg.png')").css('padding-top','140px');
	})

	$('.productivity').mouseenter(function(){
		$('.productivity > p').hide();
		$('.productivity').css('background-image',"url('../web/images/productivity-bg-green.png')").css('padding-top','40px');
		$('.productivity').prepend('<p class="reaction-header">PRODUCTIVITY</p>').append('<ul class="reaction-list"><li>Cloud storage system</li><li>Secure documents storing and more</li></ul>');
	})
	$('.productivity').mouseleave(function(){
		$('.productivity > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.productivity').css('background-image',"url('../web/images/productivity-bg.png')").css('padding-top','140px');
	})

	$('.marketing').mouseenter(function(){
		$('.marketing > p').hide();
		$('.marketing').css('background-image',"url('../web/images/marketing-bg-green.png')").css('padding-top','40px');
		$('.marketing').prepend('<p class="reaction-header">MARKETING SOLUTION</p>').append('<ul class="reaction-list"><li>Business branding</li><li>Social Media management</li><li>Print and digital design</li><li>Copywriting and more</li></ul>');
	})
	$('.marketing').mouseleave(function(){
		$('.marketing > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.marketing').css('background-image',"url('../web/images/marketing-bg.png')").css('padding-top','140px');
	})

	$('.design').mouseenter(function(){
		$('.design > p').hide();
		$('.design').css('background-image',"url('../web/images/design-bg-green.png')").css('padding-top','40px');
		$('.design').prepend('<p class="reaction-header">DESIGN</p>').append('<ul class="reaction-list"><li>Website design</li><li>Print design</li><li>Digital/Online design and more</li></ul>');
	})
	$('.design').mouseleave(function(){
		$('.design > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.design').css('background-image',"url('../web/images/design-bg.png')").css('padding-top','140px');
	})

	$('.condo').mouseenter(function(){
		$('.condo > p').hide();
		$('.condo').css('background-image',"url('../web/images/condo-bg-green.png')").css('padding-top','40px');
		$('.condo').prepend('<p class="reaction-header">CONDO MANAGEMENT</p>').append('<ul class="reaction-list"><li>Facilities booking</li><li>Secure cloud document storage</li><li>Visitors listing</li><li>Personal Data Protection Act compliance and more</li></ul>');
	})
	$('.condo').mouseleave(function(){
		$('.condo > p').show();
		$('.reaction-header').remove();
		$('.reaction-list').remove();
		$('.condo').css('background-image',"url('../web/images/condo-bg.png')").css('padding-top','140px');
	})
	//------------------------------------------------------------

	$('a').click(function(){
		$('.selected').removeClass('selected');
		$(this).parent().addClass('selected');
	});



});

