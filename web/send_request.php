<?php
require "../web/js/mailer/PHPMailerAutoload.php";

$emailPattern = '/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i';
$namePattern = '/^[a-zA-Z\-\s]{3,}$/';
$phonePattern = '/^[\d\s]{5,}$/';
$messagePattern = '/.{3,}/';

$to = 'admin@businessbuddy.sg';

//Create a new PHPMailer instance
$mail = new PHPMailer;
// Set PHPMailer to use the sendmail transport
//$mail->isSendmail();
$mail->IsSMTP();

$mail->SMTPDebug = 2;

$mail->SMTPAuth = true;

$mail->Host = "mail.businessbuddy.com.sg";

$mail->Port = 25;

$mail->Username = "demo2@businessbuddy.com.sg";

$mail->Password = "G9nzRgpdDeQ2Tf";

//Set who the message is to be sent from
if(preg_match($emailPattern, trim($_POST['email'])) && preg_match($namePattern, trim($_POST['name'])) && preg_match($messagePattern, trim($_POST['message']))){
    $mail->setFrom($_POST['email'],$_POST['email']);
    $mail->addReplyTo($_POST['email'],$_POST['name']);
    $data = $_POST['message'];
    if ($_POST['phone']!="" && preg_match($emailPattern, trim($_POST['email']))) {
        $data .= "<br/><p>Phone number: ".$_POST['phone']."</p>";
    }
    $mail->msgHTML($data);
}
//$mail->setFrom($fromEmail, $fromName);
//Set an alternative reply-to address
//$mail->addReplyTo($fromEmail, $fromName);

//Set who the message is to be sent to
$mail->addAddress($to, "Admin");
//$mail->addBCC($to);
//Set the subject line
$mail->Subject = 'From BB Contact Form';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body

//$mail->msgHTML($data);
//Replace the plain text body with one created manually
$mail->AltBody = 'From BB Contact Form';
//Attach an image file

if (!$mail->Send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}