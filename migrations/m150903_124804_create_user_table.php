<?php

use yii\db\Schema;
use yii\db\Migration;

class m150903_124804_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', ['id'=>'pk', 'name'=>'string NOT NULL', 'password'=>'string NOT NULL']);
    }

    public function down()
    {
        $this->dropTable('user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
