<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Carousel;
use yii\grid\GridView;
use app\Models\Promotion;
/* @var $this yii\web\View */

$this->title = 'Promotion';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<style>
/* Top-menu-Hint*/
li.active {
    background: none;
    background-color: #FFFFFF;
    border-bottom: 3px solid #71BD45;
    color: #71BD45;
}
li.active > a {
    color: #71BD45;
}
/*-----------------------------------------*/
.promotion-header > h1{
    
    margin-top: 75px;
    text-align: center;
    font-size: 32px;
    font-weight: 200;
    margin-bottom: 5px;
}
.promotion-header > h1 > span {
    color: #71BD45;
}
/*Description*/
.slide-description {
    padding: 50px;
    padding-top: 30px;
    padding-bottom: 5px;
    width: 80%;
    margin: 0 auto;
}
.slide-description-content {
    border: 1px solid #BCBCBC;
    border-radius: 5px;
}
.slide-description-content-left {
    padding-top: 25px;
    font-size: 16px;
    text-align: center;
    color: #71BD45;
    text-align: left;
}
.slide-description-content-right {
    padding-top: 25px;
    padding-left: 2px;
    padding-right: 10%;
    font-size: 14px;
    color: #7C7C7D;
}
/*-----------------------------------------*/
/* Buttons*/
.button-set {
    margin-top: 20px;
    margin-bottom: 10px;

    color: #7C7C7D;
}
.contact-button,
.request-button {
    border-radius: 2px;
    display: inline;
    margin-left: 7px;
    margin-right: 7px;
    font-family: 'Helvetica';
    width: 150px;
    height: 35px;
    color: white;
    font-weight: 200;
    font-size: 16px;
    background-color: #71BD45;
}
.contact-button {
    margin-top: 0;
    width: 100px;
}
/*-----------------------------------------*/
    </style>
</head>
<div class="site-index">
<div class="row">
    <div class="col-md-12 promotion-header">
        <h1>Our <span>Promotions</span></h1>
    </div>
</div>
    <?php
    $promotions = \app\models\Promotion::find()->all();
    foreach ($promotions as $promotion) { ?>
<div class="row slide-description">
    <div class="col-md-12 slide-description-content">
        <div class="col-md-2 slide-description-content-left">
            <p><?= $promotion->title ?></p>
        </div>
        <div class="col-md-10 slide-description-content-right">
            <?= $promotion->content ?>
        </div>
        <div class="col-md-offset-2 col-md-10 button-set">
        <p>
            <?php if($promotion->button ==1){ ?>
            <a class="btn request-button" href="<?php echo \Yii::$app->urlManager->createUrl(['/site/index','#'=>'contactus']);?>">request a demo</a>
            <?php } ?>
        </p>    
        </div>
    </div>
</div>
    <?php
    }
    ?>
</div>