<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Portfolio';
?>
<head>
<style>
/* Top-menu-Hint*/
li.active {
    background: none;
    background-color: #FFFFFF;
    border-bottom: 3px solid #71BD45;
    color: #71BD45;
}
li.active > a {
    color: #71BD45;
}
/*-----------------------------------------*/
/*---------Header-------------------*/
.site-portfolio {
    margin-top: 135px;
    padding-left: 10%;
    padding-right: 10%;
}
.portfolio-header > h1 {

    margin-top: 75px;
    text-align: center;
    font-size: 32px;
    font-weight: 200;
    margin-bottom: 25px;
}
.portfolio-header > h1 > span {
    color: #71BD45;
}
/*-----------------------------------------*/
/*---------Tags-------------------*/
.tag-list {
    padding-left: 5%;
    padding-right: 5%;
}
.tag-list button {
    margin-right: 10px;
    padding: 5px;
    padding-left: 10px;
    padding-right: 10px;
    background-color: #DBD9D6;
    margin-bottom: 20px;
    border: none;
    border-radius: 4px;
    color: white;
}
.tag-list button:hover {
    background-color: #71BD45;
}
*:hover{
    transition: 0.3s;
}
/*-----------------------------------------*/
/*---------Items-------------------*/
.portfolio-items {
    min-width: 100%;
    padding-left: 5%;
    padding-right: 5%;
}
.portfolio-img {
    max-width: 100%;
}
.list-item {
    min-width: 21%;
    max-width: 21%;
    border: 1px solid #DBD9D6;
    border-radius: 4px;
    margin-bottom: 15px;
    /*margin-right: 15px;*/

}
.item-content {
    padding-left: 15px;
    padding-right: 15px;
    padding-bottom: 10px;
}
.tag {
    font-size: 13px;
    height: 22px;
    padding-left: 5px;
    padding-right: 5px;
    background-color: #DBD9D6;
    margin-bottom: 10px;
    border: none;
    border-radius: 4px;
    color: white;
}
.item-content h4:hover {
    color: #71BD45;
    cursor: pointer;
}
.tag:hover {
    background-color: #8C8D8E;
}
/*-----------------------------------------*/
/*Modal post*/
.modal-dialog {
    width: 80%;
}
.modal-header {
    border: none;
}
.modal-img {
    width: 100%;
    margin-bottom: 20px;
    z-index: -1;
}
.modal-cont {
    padding-left: 50px;
    padding-right: 50px;
    z-index: 4;
}
.post-modal-img {
    height: 100%;
    position: relative;
    padding-bottom: 0;
}
.post-title {
    position: absolute;
    bottom: 10px;
    display: inline-block;
    padding-left: 10%;
    padding-right: 10%;
    color: white;
    font-size: 40px;
    background-color: rgba(0, 0, 0, 0.2);
    width: 100%;
    height: 60%;
    margin-bottom: 10px;
    padding-bottom: 0;
    padding-top: 40px;

}
/************************/
</style>
</head>

<div class="site-portfolio">
    <div class="portfolio-header">
        <h1>Our <span><?= Html::encode($this->title) ?></span></h1>
    </div>
    <div class="tag-list">
        <?php
        $tags = \app\models\Tag::find()->asArray()->all();
        foreach ($tags as $tag){
            echo "<button>".$tag['tag']."</button>";
        }
        ?>
    </div>
    <div class="portfolio-items">
        <?php
        $posts = \app\models\Portfolio::find()->all();
        foreach ($posts as $post) {
            $id = str_replace(' ','',$post->title);
            ?>
        <div class="list-item" id="<?= $id ?>">
            <img class="portfolio-img" src="../web/post/<?= $post->file ?>">
            <div class="item-content">
                <h4><?= $post->title ?></h4>
                <button class="tag"><?= $post->tag ?></button>
                <p><?php echo substr($post->content, 0, 150); ?></p>
            </div>
        </div>
            <div class="modal fade" id="m<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="post-modal-img">
                                <img class = "modal-img" src="../web/post/<?= $post->file ?>">
                                <h3 class="post-title"><?= $post->title ?></h3>
                            </div>
                            <div class = "modal-cont">
                                <?= $post->content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>
<script>
var $grid = $('.portfolio-items').masonry({
    itemSelector: '.list-item',
    gutter: 15
    });
$grid.masonry();
$(document).height($(document).height()+1);
$(window).on('load', function(){
    $grid.masonry();
});
$('.tag-list button').click(function(){
    console.log($(this).text());
    var tagName = $(this).text();
    $.ajax({
        url: '../web/portfolio_script.php',
        type: 'post',
        data: {
            tag: tagName
        },
        success: function (response) {
            $('.portfolio-items').empty().append(response);
            $grid.masonry('reloadItems');
            $grid.masonry();
        }
    });

});
$(document).on('click','.list-item',function () {
    console.log($(this).attr('id'));
    $('#m' + $(this).attr('id')).modal();

});
</script>