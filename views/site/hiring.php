<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Carousel;
use yii\grid\GridView;
use app\Models\Promotion;
/* @var $this yii\web\View */

$this->title = 'Promotion';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<style>
.hiring-header {
    margin-top: 70px;
}
.slide-description {
    margin-top: 30px;
    padding-left: 10%;
    padding-right: 10%;
}
.hiring {
    color: #71BD45;
    margin-bottom: 30px;
}
.job-description {
    border: 1px solid #BCBCBC;
    border-radius: 5px;
    min-height: 600px;
}
.positions li {
    height: 26px;
}
.positions li:hover {
    cursor: pointer;
}
/* Maximum magic label*/
.mark {
    position: absolute;
    top: 50px;
    right: 0px;
    background: #71BD45;
    color: white;
    font-weight: 300;
    padding-bottom: 7px;
    padding-top: 7px;
    padding-right: 10px;
}
.triangle {

    background: green;
    border-radius: 20px;
    padding: 0px;
    position: relative;
    color: #71BD45;
}
.triangle::after {
    height: 20px;
    content: '';
    position: absolute;
    left: -44px; bottom: -80px;
    border: 14px solid transparent;
    border-right: 15px solid #71BD45;
}
/*************************************************/
.benefits {
    background-color: #F1F2F3;
    margin-top: 15px;
    min-height: 150px;
    padding-left: 10%;
    padding-right: 10%;
}
.benefits-header {
    padding-right: 70px;
}
.job-description {
    padding: 30px 30px 40px 40px;
}
.description-header {
    font-weight: bold;
    margin-bottom: 25px;
}
.chosen {
    color: #71BD45;
}
.footer {
    margin-top: 0;
}
.perk{
    min-height: 100px;
    padding-top: 20px;
    text-align: center;
    margin-bottom: 10px;
}
</style>
</head>
<div class="site-index">
    <div class="row">
        <div class="col-md-12 hiring-header">
        </div>
    </div>
    <div class="row slide-description">
        <div class="col-md-12">
            <div class="col-md-3">
                <h3 class="hiring">We are hiring</h3>
                <ul class="positions">
                <?php
                $vacancies = \app\models\Vacancy::find()->orderBy('id ASC')->all();
                foreach ($vacancies as $vacancy) { ?>
                    <li class="title" id="<?= $vacancy->title ?>"><?= $vacancy->title ?></li>
                <?php } ?>
                </ul>
            </div>
            <div class=" col-md-9 job-description">
                <!--label class="triangle"></label-->
                <h4 class="description-header"><?= $vacancies[0]->title ?></h4>
                <p class="w"></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 benefits">
            <div class="col-md-3 benefits-header">
                <h3>Perks and Benefits</h3>
            </div>
            <div class="col-md-9 benefits-images">
                <?php
                $perks = ['friendly'=>$vacancies[0]->friendly, 'office'=>$vacancies[0]->office, 'compensation' => $vacancies[0]->compensation, 'commission'=>$vacancies[0]->commission];
                if ($perks['office']!=0) echo "<div class=\"col-md-4 perk\"><img src=\"../web/images/office.png\"><h5>Great office location</h5></div>";
                if ($perks['friendly']!=0) echo "<div class=\"col-md-4 perk\"><img src=\"../web/images/friendly.png\"><h5>Friendly Team</h5></div>";
                if ($perks['compensation']!=0) echo "<div class=\"col-md-4 perk\"><img src=\"../web/images/compensation.png\"><h5>Fair compensation</h5></div>";
                echo "<div class=\"col-md-4 perk blank\"></div>";
                if ($perks['commission']!=0) echo "<div class=\"col-md-4 perk\"><img src=\"../web/images/comission.png\"><h5>Highest Commission rate in industry</h5></div>";
                ?>

            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.title:first').addClass('chosen');
    $.ajax({
        url: '../web/job_script.php',
        type: 'post',
        data: {title : $('.title:first').attr('id')},
        success: function(response){
            $('.w').html(response);
        }
    });
});
$('.title').click(function(){
    $('li').removeClass('chosen');
    $(this).addClass('chosen');
    $('.w').empty();
    $('.description-header').text($(this).attr('id'));
    $.ajax({
        url: '../web/job_script.php',
        type: 'post',
        data: {title : $(this).attr('id')},
        success: function(response){
            $('.w').html(response);
        }
    });
    $.ajax({
        url: '../web/perk_script.php',
        type: 'post',
        data: {title : $(this).attr('id')},
        success: function(response){
            $('.benefits-images').empty();
            $('.benefits-images').append(response);
        }
    });
});


</script>