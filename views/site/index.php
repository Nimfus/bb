<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\Carousel;
use yii\bootstrap\ActiveForm;
use yii\widgets\Redactor;

/* @var $this yii\web\View */

$this->title = 'Business Buddy';
?>
<style>
    .red {
        border: 1px solid red;
    }
    .sent {
        margin-top: 20px;
        text-align: center;
        padding-top: 15%;
    }
    .sent-img {
        display: block;
        height: 70px;
        width: 60px;
        margin: 0 auto;
    }
    .message-sent {
        display: block;
        margin: 0 auto;
    }

</style>
<div class="site-index">
<div class="row">
    <div class="col-md-12 main-slide">
        <div class="col-md-4 main-slide-text">
            <h1>We make <span>your success</span> our business</h1>
            <p>Let us help you increase your profits and reduce business costs with automation</p>
            <span id="solutions"></span>
            <a class="btn header-button" href="<?php echo \Yii::$app->urlManager->createUrl(['started']);?>">Let's get started</a>
            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 solutions-slide-header">
            <h1>Great <span>solutions</span> for your business</h1>
    </div>
</div>    
<div class="row">    
    <div class="col-md-4 solutions-slide-text">
            <p>Business Buddy is a company that helps entrepreneurs run their business more easily and efficiently. At 
        the heart of everything we do, we are commited to your business' success.</p>
    </div>    
</div>
<div class="row img-grid">
    <div class="img-grid-insider">
        <div class="col-md-2 solutions creation"> 
            <p>WEBSITE</p>
            <p>CREATION</p>
        </div>
        <div class="col-md-2 solutions mobile"> 
            <p>MOBILE</p>
            <p>APPS</p>
        </div>
        <div class="col-md-2 solutions customer"> 
            <p>CUSTOMER</p>
            <p>SOLUTIONS</p>
        </div>
        <div class="col-md-2 solutions employee"> 
            <p>EMPLOYEE</p>
            <p>MANAGEMENT</p>
        </div>
    </div>
</div>
<div class="row img-grid">
    <div class="img-grid-insider">
        <div class="col-md-2 solutions productivity"> 
            <p>PRODUCTIVITY</p>
        </div>
        <div class="col-md-2 solutions marketing"> 
            <p>MARKETING</p>
            <p>SOLUTIONS</p>
        </div>
        <div class="col-md-2 solutions design"> 
            <p>DESIGN</p>
        </div>
        <div class="col-md-2 solutions condo"> 
            <p>CONDO</p>
            <p>MANAGEMENT</p>
        </div>
    </div>
</div>
<div class="row">
    <?php echo \Yii::$app->view->renderFile('@app/views/site/index/slider1.php');?>
</div>
<div class="row" id="clients">
    <?php echo \Yii::$app->view->renderFile('@app/views/site/index/slider2.php');?>
</div>
<div class="row">
    <div class="col-md-4 solutions-slide-header">
            <h1>Our <span>Portfolio</span></h1>
    </div>
</div>
</div>
<div class="row portfolio-grid">
    <div class="portfolio-grid-insider">
        <div class="col-md-2 portfolio products"> 
            <img id="products" src="../web/images/menu-products.png">
        </div>
        <div class="col-md-2 portfolio designs"> 
            <img id="designs" src="../web/images/menu-designs.png">
        </div>
        <div class="col-md-2 portfolio websites"> 
            <img id="websites" src="../web/images/menu-websites.png">
        </div>
        <div class="col-md-2 portfolio other"> 
            <img id="other" src="../web/images/menu-other.png">
        </div>
    </div>
</div> 
<div class="row" id="clients">
    <span id="employee"></span>
    <?php echo \Yii::$app->view->renderFile('@app/views/site/index/slider3.php');?>
</div>
<div class="row">
    <div class="coll-md-12 solutions-slide-header">
        <h1><span>Contact</span> Us</h1>
    </div>
</div>  
<div class="row">
    <div class="col-md-6 contact-left">
        <p class="first">If you have a project you want to discuss, get in touch with us:</p>
        <p>Business Buddy Pte Ltd <br/>
        3 Anson Road, #25-03<br/>
        Springleaf Tower<br/>
        Singapore 079909</p>
        <p>+65 6708 9308<br/>
        <a href="mailto:admin@businessbuddy.sg">admin@businessbuddy.sg</a></p>
        <div id="map"></div>
    </div>
    <div class="col-md-6 contact-right">
        <?php
        $model = new \app\models\ContactusForm;
        $form = ActiveForm::begin([
        'id' => 'contactus-form',
        //'method' => 'get',
        //'action' => ['admin/index'],
        'options' => ['class' => 'form-horizontal'],
    ]) ?>
        <?= $form->field($model,'username', ['template' => '<div class=\"form-group\">{label}<div class="col-md-9">{input}</div><span id="nameError"></span> </div>',
            ])->textInput(['maxlength' => 255, 'class' => 'contact-name-input'])->label('Name<span style="color:#71BD45;"> *</span>', ['class' => 'col-md-3 contact-name-label']) ?>

        <?= $form->field($model, 'email', ['template' => '<div class=\"form-group\">{label}<div class="col-md-9">{input}</div><span id="emailError"></span></div>',
        ])->textInput(['maxlength' => 255, 'class' => 'contact-email-input'])->label('Email<span style="color:#71BD45;"> *</span>', ['class' => 'col-md-3 contact-email-label']) ?>

        <?= $form->field($model, 'message', ['template' => '<div class=\"form-group\">{label}<div class="col-md-9">{input}</div><span id="messageError" style="padding-left:200px;"></span></div>',
        ])->label('Message<span style="color:#71BD45;"> *</span>', ['class' => 'col-md-3 contact-message-label'])->textarea(['class' => 'contact-message-input', 'id' => 'message']) ?>

        <div class="form-group">
            <div class="col-md-offset-3 col-md-9">
                <?= Html::button('SEND', ['class'=>'btn contact-button']) ?>
                <span class="contact-error"></span>
            </div>
        </div>
        <?php ActiveForm::end() ?>
</div>
</div>
<script>
    $('#message').focus(function(){
        $('#message').css('border','1px solid #D0D0D0');
        $('#messageError').text('');
    });
    $('.contact-name-input').focus(function(){
        $('.contact-name-input').removeClass('red');
        $('#nameError').text('');
    });
    $('.contact-email-input').focus(function(){
        $('.contact-email-input').removeClass('red');
        $('#emailError').text('');
    });
    $('.contact-button').click(function(){
        if(checkfields()==0) {
            $.ajax({
                url: '../web/send_request.php',
                type: 'post',
                data: {
                    name: $('.contact-name-input').val(),
                    email: $('.contact-email-input').val(),
                    message: $('.contact-message-input').val()
                },
                success: function (response) {
                    $('#contactus-form').empty();
                    $('#contactus-form').html('<div class="sent"><img class="sent-img" src="../web/images/green-tick.png"><h4 class="message-sent">Your message was successfully sent</h4></div>');
                }
            });
        }
    });
    function checkfields(){
        var errors=0;
        var emailPattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        var namePattern = /^[a-zA-Z\-\s]{3,}$/;
        var messagePattern = /.{3,}/;
        if(!emailPattern.test($('.contact-email-input').val())){
            $('#emailError').text('Invalid e-mail address').css('color','red');
            $('.contact-email-input').addClass('red');
            errors++;
        }
        if(!namePattern.test($('.contact-name-input').val())){
            $('#nameError').text('Input correct name please').css('color','red');
            $('.contact-name-input').addClass('red');
            errors++;
        }
        if(!messagePattern.test($('#message').val())){
            $('#messageError').css('padding-left','200px;');
            $('#messageError').text('Message can\'t be emty').css('color','red');
            $('#message').css('border','1px solid red');
            errors++;
        }
        return errors;
    }
</script>