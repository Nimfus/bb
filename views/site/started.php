<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Carousel;
use yii\grid\GridView;
use app\Models\Promotion;
use app\models\Product;
/* @var $this yii\web\View */

$this->title = 'Promotion';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<style>
.site-index {
    font-family: 'Helvetica';
    margin-top: 70px;
}
.product-header {
    text-align: center;
}
.top-level {
    padding-right: 10%;
    padding-left: 10%;
}
.product-item-container {
    min-height: 300px;
    min-width: 250px;
    padding: 15px 15px 15px 15px;

}
.product-item {
    margin:  auto;
    min-width: 218px;
    max-width: 300px;
    min-height: 268px;
    padding-top: 10px;
    text-align: center;
    background-position-y: 100%;
    background-position-x: 50%;
    background-repeat: no-repeat;
    background-size: 75%;
}
.product-item h4 {
    padding-top: 10px;
    color: #71BD45;
    margin-bottom: 0px;
}
.content {
    text-align: left;
    padding-left: 15px;
    padding-right: 5px;
    color: white;;
    display: none;
    min-height: 100%;
    cursor: pointer;
}
.cover {
    width: 100%;
    min-height: 268px;
}
.learn-button {
    color: #71BD45;
    padding-top: 8px;
    position: absolute;
    bottom: 35px;
    left: 30%;
    background: white;
    height: 35px;
    width: 120px;
    text-decoration: none;
    display: none;
    cursor: pointer;
}
/*Contact*/
.contacts {
    background: #71BD45;
    margin-top: 20px;
}
.contact-header {
    text-align: center;
    color: white;
    background: #71BD45;
}
.contact {
    background: #71BD45;
}
.left-side {
    padding-right: 60px;
    padding-top: 40px;
}
.right-side {
    padding-top: 40px;
}
label {
    color: white;
    font-weight: 200;
    width: 35%;
    text-align: right;
    margin-right: 50px;
    margin-bottom: 5px;
}
.col-md-2 label {
    color: white;
    font-weight: 200;
    width: 100%;
    text-align: right;
    margin-right: 50px;
    margin-bottom: 5px;
}
label span {
    color: black;
}
input {
    width: 50%;
    margin-bottom: 5px;
    opacity: 0.6;
    height: 30px;
}
textarea:focus,
input:focus {
    opacity: 0.8;
}
textarea {
    resize: none;
    opacity: 0.6;
    width: 80%;
    height: 100px;
}
.contact-send {
    color: #71BD45;
    padding-top: 6px;
    background: white;
    height: 35px;
    width: 100px;
    text-decoration: none;
    cursor: pointer;
    border: none;
    margin-bottom: 20px;
}
.contact-send:hover {
    color: black;
}
.contact-footer {
    margin-top: 10px;
    padding-left: 10%;
    font-weight: 100;
}
.contact-footer-lower {
    text-align: center;
}
/*--------------*/
a:visited,
a:active,
a:hover {
    text-decoration: none;
    color: #71BD45;
}
.e3 {
    font-size: 12px;
    display: block;
    padding-left: 40%;
}
.e4 {
    font-size: 12px;
    display: block;
    padding-left: 10%;
}
.red {
    border: 1px solid red;
}
.sent {
    margin-top: 20px;
    text-align: center;
    margin-bottom: 100px;
}
.sent-img {
    display: block;
    height: 70px;
    width: 60px;
    margin: 0 auto;
}
.message-sent {
    display: block;
    margin: 0 auto;
    color: white;
}
    .footer {
        margin-top: 0;
    }
</style>
</head>
<div class="site-index">
    <div class="row">
        <div class="col-md-12 product-header">
            <h1>Solutions</h1>
        </div>
    </div>

    <div class="row top-level">
        <?php
        $products = \app\models\Product::find()->all();
        foreach ($products as $product) { ?>
        <div class="col-md-4 product-item-container">
            <div class="product-item" style="background-image: url('../web/post/<?=$product->file ?>')">
                <div class="cover">
                    <h4><?= $product->title ?></h4>
                    <div class="content">
                        <?= $product->description ?>
                    </div>
                    <?php if ($product->url != ""){
                       echo "<a class=\"learn-button\" href='.$product->url.'>learn more</a>";
                    }?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-12 contacts">
            <div class="col-md-12 contact-header">
                <h3>Unsure what to look for?</h3>
                <h4>Fill up the contact form below and we will get in touch with you</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 contact">
            <div class="col-md-6 left-side">
                <div class="name">
                    <label for="name">Name<span> *</span></label>
                    <input type="text" name="name" id="name">
                    <span class="e3"  id="nameError"></span>
                </div>
                <div class="phone">
                    <label for="phone">Phone<span> *</span></label>
                    <input type="text" name="phone" id="phone">
                    <span class="e3"  id="phoneError"></span>
                </div>
                <div class="email">
                    <label for="email">E-mail<span> *</span></label>
                    <input type="email" name="email" id="email">
                    <span class="e3" id="emailError"></span>
                </div>
            </div>
            <div class="col-md-6 right-side">
                <div class="col-md-2">
                    <label for="message">Message<span> *</span></label>
                </div>
                <div class="col-md-10">
                    <textarea  name="message" id="message1"></textarea>
                    <span class="e4"  id="messageError"></span>
                </div>
            </div>
            <div class="col-md-12 contact-footer">
                <p style="color: white;"><span style="color: black;">* </span>compulsory</p>
            </div>
            <div class="col-md-12 contact-footer-lower">
                <button class="contact-send">send</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('.product-item').mouseenter(function(){
       $(this).find('.cover').css('background','#71BD45').css('opacity','0.95').css('transition','0.3s');
       $(this).find('h4').css('color','white');
       $(this).find('.content').css('display','inline');
       $(this).find('.learn-button').css('display','inline');
    });
    $('.product-item').mouseleave(function(){
        $(this).find('h4').css('color','#71BD45');
        $(this).find('.content').css('display','none');
        $(this).find('.learn-button').css('display','none');
        $(this).find('.cover').css('background','transparent').css('transition','0.3s');
    });

    $('#email').focus(function(){
        $('#email').css('border','1px solid #D0D0D0');
        $('#emailError').text('');
    });
    $('#name').focus(function(){
        $('#name').removeClass('red');
        $('#nameError').text('');
    });
    $('#phone').focus(function(){
        $('#phone').removeClass('red');
        $('#phoneError').text('');
    });
    $('#message1').focus(function(){
        $('#message1').removeClass('red');
        $('#messageError').text('');
    });
    $('.contact-send').click(function(){
        if(checkfields()==0) {
            $.ajax({
                url: '../web/send_request.php',
                type: 'post',
                data: {
                    name: $('#name').val(),
                    phone: $('#phone').val(),
                    email: $('#email').val(),
                    message: $('#message1').val()
                },
                success: function (response) {
                    $('.contact').empty();
                    $('.contact').html('<div class="sent"><img class="sent-img" src="../web/images/green-tick-w.png"><h4 class="message-sent">Your message was successfully sent</h4></div>');
                }
            });
        }
    });
    function checkfields(){
        var errors=0;
        var emailPattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        var namePattern = /^[a-zA-Z\-\s]{3,}$/;
        var phonePattern = /^[\d\s]{5,}$/;
        var messagePattern = /.{3,}/;
        if(!emailPattern.test($('#email').val())){
            $('#emailError').text('Invalid e-mail address').css('color','red');
            $('#email').addClass('red');
            errors++;
        }
        if(!namePattern.test($('#name').val())){
            $('#nameError').text('Input correct name please').css('color','red');
            $('#name').addClass('red');
            errors++;
        }
        if(!phonePattern.test($('#phone').val())){
            $('#phoneError').text('Field should consist only digits and whitespaces').css('color','red');
            $('#phone').addClass('red');
            errors++;
        }
        if(!messagePattern.test($('#message1').val())){
            $('#messageError').css('padding-left','200px;');
            $('#messageError').text('Message can\'t be emty').css('color','red');
            $('#message1').addClass('red');
            errors++;
        }
        return errors;
    }
</script>