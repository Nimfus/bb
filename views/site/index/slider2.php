<style>
    #slider2 {
        z-index: 199;
    }
    .tt {

        position: relative;
        margin: 0;
    }
    .ii-right-top {
        float: left;
        position: absolute;
        background-image: url("../web/images/bubble-right-top.png");
        opacity: 0.7;
        background-size: 90%;
        background-repeat: no-repeat;
        height: 500px;
        width: 420px;
        z-index: 99999;
        top: 100px;
        left:60%;
        right: 20%;


        padding: 210px 100px 110px 60px;
    }
    .ii-left-top {
        float: left;
        position: absolute;
        background-image: url("../web/images/bubble-left-top.png");
        opacity: 0.7;
        background-size: 90%;
        background-repeat: no-repeat;
        height: 500px;
        width: 420px;
        z-index: 99999;
        top: 100px;
        left: 15%;

        padding: 210px 100px 110px 60px;
    }
    .ii-right-bottom {
        float: left;
        position: absolute;
        background-image: url("../web/images/bubble-right-bottom.png");
        opacity: 0.7;
        background-size: 90%;
        background-repeat: no-repeat;
        height: 500px;
        width: 420px;
        z-index: 99999;
        top: 70px;
        left:60%;
        right: 15%;

        padding: 100px 100px 210px 60px;
    }
    .ii-left-bottom {
        float: left;
        position: absolute;
        background-image: url("../web/images/bubble-left-bottom.png");
        opacity: 0.7;
        background-size: 90%;
        background-repeat: no-repeat;
        height: 500px;
        width: 420px;
        z-index: 99999;
        top: 70px;
        left: 15%;

        padding: 100px 100px 210px 60px;
    }
    .ii-right-top p,
    .ii-right-bottom p,
    .ii-left-bottom p,
    .ii-left-top p {
        color: white;
        opacity: 1;
        font-size: 22px;
    }
    .waiting {
        display: none;
    }
</style>
<div class="col-md-12 slider2">
    <div id="slider2" class="carousel slide">
        <!-- Indicators -->
        <?php
        /*$servername = "127.0.0.1";
          $username = "devbusin_user";
          $password = ";7iJgtS!+#=0";
          $dbname = "devbusin_yii2basic";*/
        $servername = "127.0.0.1";
        $username = "nimfus";
        $password = "";
        $dbname = "yii2basic";

        $images = array_diff(scandir('../web/carousel/slider2/'), array('..', '.','.DS_Store'));
        $slider = 'slider2';

        $slides = [];
        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT * FROM slide WHERE slider = '$slider' ORDER BY order_number");
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $slides = $stmt->fetchAll();


        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;

        ?>
        <ol class="carousel-indicators">
            <?php
            $k=0;
            foreach ($slides as $slide) {
                if ($slide['showw'] === 'true') {
                    if ($k == 0) {
                        echo '<li data-target="#slider2" data-slide-to="' . $k . '" class="active"></li>';
                    } else {
                        echo '<li data-target="#slider2" data-slide-to="' . $k . '"></li>';
                    }
                    $k++;
                }
            }
            ?>
        </ol>
        <!-- Carousel items -->
        <div class="carousel-inner">
            <?php
            $i=0;
            foreach ($slides as $slide) {
                $text = $slide['bubble_text'];
                $position = $slide['bubble_position'];
                $rotation = $slide['bubble_rotation'];
                $pos = $position."-".$rotation;
                if ($slide['showw'] === 'true') {
                    if ($i == 0) {
                        echo '<div id="slider2-'.$i.'" class="active item"><img class="tt" src="../web/carousel/slider2/' . $slide['title'] . '"><div id="top-'.$i.'" class="carousel-caption ii-'.$pos .'"><p>'.$text.'</p></div></div>';
                    } else {
                        echo '<div id="slider2-'.$i.'" class="item"><img class="tt" src="../web/carousel/slider2/' . $slide['title'] . '"><div id="top-'.$i.'" class="carousel-caption ii-'.$pos .'"><p>'.$text.'</p></div></div>';
                    }
                    $i++;
                }
            }
            ?>
        </div>
        <!--div class="ii-right-top"><p>test test test</p></div-->
        <!-- Carousel nav -->
        <a class="left carousel-control" href="#slider2" data-slide="prev"><</a>
        <a class="right carousel-control" href="#slider2" data-slide="next">></a>
    </div>
</div>