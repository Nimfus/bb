<style>
    .slider1 {
        min-width: 100%;
        background-color: #F1F1F1;
        position: relative;
        min-height: 800px;
        max-height: 800px;
        z-index: 1;
    }
    #slider1 {
        position: absolute;
        top: 40px;
        left: 60px;
        width: 690px;
        height: 390px;
        z-index: 10;
    }
    .apple-left {
        padding-right: 10%;
        padding-top: 150px;
    }
    .right-apple {
        position: relative;
        top: 120px;
        z-index: 1;
    }
    .right-apple img {
        position: absolute;
        top: 10px;
        left: 30px;
        z-index: 2;
        background: transparent;
    }
    .lsa {
        margin-bottom: 50px;
        text-align: left;
        padding-left: 10px
    }
    .lsa span {
        color: #71BD45;
    }
    .ppa {
        font-size: 20px;
        text-align: left;
        padding-left: 10px;
    }
    #slider1 .carousel-control {
        top: 135%;
    }
    #slider1 .right {
        right: 0;
    }
    .carousel-inner {
        height: 600px;
    }
    .kk {
        margin-left: -30px;
        margin-top: -10px;
        min-height: 388px;
        max-height: 388px;
        min-width: 690px;
        max-width: 690px;
    }

</style>
<div class="col-md-12 slider1">
    <div class="col-md-offset-1 col-md-4 apple-left">
        <h1 class="lsa">Awesome <span>design</span> and <span>user-friendly</span> interface</h1>
        <p class="ppa">We believe that great design is extremely important for a business. Great design attracts customers.<br/>
        User-friendly interface is important to keep your customers and allow them to enjoy what you offer.</p>
    </div>
    <div class="col-md-7 right-apple">
        <img src="../web/images/mac.png">
    <div id="slider1" class="carousel slide">
        <!-- Carousel items -->
        <div class="carousel-inner">
            <?php
                $images = array_diff(scandir('../web/carousel/slider1/'), array('..', '.','.DS_Store'));
                $i=0;
                foreach ($images as $image) {
                    if ($i==0) {
                        echo '<div class="active item"><img class="kk" src="../web/carousel/slider1/' . $image . '"></div>';
                    }else{
                        echo '<div class="item"><img class="kk" src="../web/carousel/slider1/' . $image . '"></div>';
                    }
                    $i++;
                }
            ?>
        </div>
        <!-- Carousel nav -->
        <a class="left carousel-control" href="#slider1" data-slide="prev"><</a>
        <a class="right carousel-control" href="#slider1" data-slide="next">></a>
    </div>
    </div>
</div>