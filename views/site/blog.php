<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\Models\Post;
$this->title = 'Blogs';
?>

<style>
/* Top-menu-Hint*/
li.active {
    background: none;
    background-color: #FFFFFF;
    border-bottom: 3px solid #71BD45;
    color: #71BD45;
}
li.active > a {
    color: #71BD45;
}
/*-----------------------------------------*/
body {
    margin-top: 50px;
    max-height: 75%;
}
.blog-content {
    overflow: hidden;
    max-height: 20%;
}
.posts{
    overflow: auto;
    max-height: 500px;
}
.post {
    padding: 20px;
    padding-top: 0px;
    padding-right: 30px;
}
.post h4 {
    font-weight: bold;
    max-height: 80%;
}
.post h4:hover {
    color: #71BD45;
    cursor: pointer;
}
.post-img {
    width: 80%;
    border-radius: 4px;
}
.featured-post-container {
    min-height: 500px;
    max-height: 500px;
    padding: 0;
    border-bottom-right-radius: 5px;
    border-top-right-radius: 5px;
}
.featured-post {
    padding: 0;
}
/* Maximum magic label*/
.mark {
    position: absolute;
    top: 50px;
    right: 0px;
    background: #71BD45;
    color: white;
    font-weight: 300;
    padding-bottom: 7px;
    padding-top: 7px;
    padding-right: 10px;
}
.triangle {
    background: green;
    border-radius: 20px;
    padding: 0px;
    position: relative;
    color: #71BD45;
}
.triangle::after {
    height: 32px;
    content: '';
    position: absolute;
    left: -36px; bottom: -18px;
    border: 18px solid transparent;
    border-right: 15px solid #71BD45;
}
/*************************************************/
.featured-img {
    width: 100%;
    padding: 0;
}
/*Modal post*/
.modal-dialog {
    width: 80%;
}
.modal-header {
    border: none;
}
.modal-img {
    width: 100%;
    margin-bottom: 20px;
    z-index: -1;
}
.modal-cont {
    padding-left: 50px;
    padding-right: 50px;
    z-index: 4;
}
.post-modal-img {
    height: 100%;
    position: relative;
    padding-bottom: 0;
}
.featured-post-title,
.post-title {
    position: absolute;
    bottom: 10px;
    display: inline-block;
    padding-left: 10%;
    padding-right: 10%;
    color: white;
    font-size: 40px;
    background-color: rgba(0, 0, 0, 0.2);
    width: 100%;
    height: 60%;
    margin-bottom: 10px;
    padding-bottom: 0;
    padding-top: 40px;

}
.featured-post-title {
    padding-top: 120px;
}
.modal-mark {
    position: absolute;
    bottom: 58.7%;
    left: 11%;
    background: #71BD45;
    color: white;
    font-weight: 300;
    padding-bottom: 7px;
    padding-top: 7px;
    padding-right: 10px;
    padding-left: 10px;
    z-index: 5;
}
/************************/
.pagination {
    visibility: hidden;
}
.ias-noneleft {
    visibility: hidden;
}
.ias-spinner {
    padding-right: 20%;
}
</style>

<div class="row blogStart">
    <div class="col-md-12 solutions-slide-header">
        <h1>Our <span><?= Html::encode($this->title) ?></span></h1>
    </div>
</div>

<div class="row blog-content">
    <div class="col-md-offset-1 col-md-8 featured-post-container">
        <?php
        $post = \app\models\Post::find()->orderBy(['time' => SORT_DESC])->where(['featured' => '1'])->one();
        ?>
            <div class="featured-post">
                <img class = "featured-img" src="../web/post/<?= $post->file ?>">
            </div>
        <label class="mark"><label class="triangle"></label>featured post</label>
        <div class="modal fade" id="featured-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="post-modal-img">
                            <img class = "modal-img" src="../web/post/<?= $post->file ?>">
                            <label class="modal-mark">featured post</label>
                            <h3 class="featured-post-title"><?= $post->title ?></h3>
                        </div>
                        <div class = "modal-cont">
                            <?= $post->content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="posts" class="col-md-3 posts">
        <div class="nne">
    <?php
        foreach ($models as $model) { ?>
            <?php
            $id = str_replace(' ','',$model->title);
            ?>
        <div class="post" id="<?= $id ?>">
            <h4><?= $model->title ?></h4>
            <img class = "post-img" src="../web/post/<?= $model->file ?>">
        </div>
        <?php
        } ?>
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
        </div>
    </div>
    <?php
    $posts = \app\models\Post::find()->all();
    foreach($posts as $post){
        $id = str_replace(' ','',$post->title);
        ?>
        <div class="modal fade" id="m<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="post-modal-img">
                            <img class = "modal-img" src="../web/post/<?= $post->file ?>">
                            <h3 class="post-title"><?= $post->title ?></h3>
                        </div>
                        <div class = "modal-cont">
                            <?= $post->content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>


<script>
    var ias = $('#posts').ias({
        container:  ".nne",
        item:       ".post",
        pagination: "#pagination",
        next:       ".next a",
        delay:      1250
    });

    ias.extension(new IASSpinnerExtension());
    ias.extension(new IASNoneLeftExtension());

    $(document).on("click",'.post',function() {
        console.log($(this).attr('id'));
        $('#m'+$(this).attr('id')).modal();
    });
    $('.featured-post').click(function() {
        console.log($(this).attr('id'));
        $('#featured-modal').modal();
    });
</script>