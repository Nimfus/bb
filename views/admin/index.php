<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\db\Query;

$this->title = 'Control Panel';
?>
<style>
    .dz-preview, .dz-file-preview, .dz-processing, .dz-error, .dz-details, .dz-progress,
    .dz-success-mark, .dz-error-mark, .dz-error-message{
        display: none;
    }
    .col-md-10 {
        border-bottom: 1px solid grey;
    }
    .img-container1,
    .img-container2,
    .img-container3 {
        display: inline-block;
        width: 210px;
        margin-right: 15px;
        text-align: center;
        margin-bottom: 20px;
    }
    .slider-img {
        margin-right: 15px;
    }
    .slider-img:hover {

    }
    .remover {
        margin-top: 5px;
        height: 21px;
        width: 80px;
        border: 1px solid grey;
        border-radius: 4px;
        padding-top: 0px;

    }
    .remover:hover {
        background-color: #FF3333;
        transient: 0.5;
    }
    .bootstrap-filestyle input {
        height: 34px;
        max-width: 110px;
    }
    .cover {
        max-height: 31px;
        margin: 0 auto;
    }
    #file1,
    #file2,
    #file3 {
        height: 27px;
        width: 100px;
        border: 1px solid #ccc;
        display: block;
        float: left;
        border-bottom-left-radius: 5px;
        border-top-left-radius: 5px;
        padding-top: 2px;
        padding-left: 10px;
        background: #ccc;
        cursor: pointer;
    }
    #file1 span,
    #file2 span,
    #file3 span {
        margin-right: 4px;
    }
    .filename1,
    .filename2,
    .filename3 {
        height: 27px;
        width: 200px;
        border: 1px solid #ccc;
        display: block;
        margin-left: 100px;
        padding-top: 2px;
        padding-left: 10px;
        border-bottom-right-radius: 5px;
        border-top-right-radius: 5px;
    }
    .uploader1,
    .uploader2,
    .uploader3 {
        position: absolute;
        left: 294px;
        margin-top: -27px;
        border: 1px solid #ccc;
        border-bottom-right-radius: 5px;
        border-top-right-radius: 5px;
        height: 27px;
        padding-bottom: 3px;
    }
    /*Settings*/
    .settings label {
        min-width: 150px;
        font-weight: 200;
    }
    .settings input {
        display: inline;
        border: 1px solid #ccc;
        border-radius: 3px;
        width: 300px;
    }
    .settings select {
        display: inline;
        width: 150px;
    }
    .settings textarea {
        display: block;
        width: 300px;
    }
    #bt {
        display: block;
        float: left;
        margin-right: 4px;
    }
    #saveBanner1,
    #saveBanner {
        margin-left: 250px;
        margin-bottom: 50px;
    }
    .choosen {
        border: 1px solid
    }
    /*------------------*/
    .loading {
        background: url('../images/gears.gif');
        background-repeat: no-repeat;
        background-position: center;

    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-offset-1 col-md-10 ttban">
            <h4>Design banner settings</h4>
            <?php
            $slider1 = array_diff(scandir('../web/carousel/slider1/'), array('..', '.','.DS_Store'));
            foreach ($slider1 as $slide){
                echo '<div class="img-container1"><img class="slider-img" width=200 src="../carousel/slider1/'.$slide.'"><button class="remover" id="slider1/'.$slide.'">Remove</button></div>';
            }
            ?>
            <div class="form-group">
                <label for="file1">Upload new banner image</label>
                <div class="cover">
                    <div id="file1"><span class="glyphicon glyphicon-folder-open dz-clickable" aria-hidden="true"></span>Select file</div>
                    <div class="filename1"></div>
                    <button class="uploader1">Upload</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-1 col-md-10 clients-banner">
            <h4>Clients banner settings</h4>
            <h5>Already uploaded banners:</h5>
            <?php
            $slider2 = array_diff(scandir('../web/carousel/slider2/'), array('..', '.','.DS_Store'));
            foreach ($slider2 as $slide){
                echo '<div class="img-container2"><img class="slider-img" width=200 src="../carousel/slider2/'.$slide.'"><button class="remover" id="slider2/'.$slide.'">Remove</button></div>';
            }
            ?>
            <h5>Choose a banner to arrange settings</h5>
            <div class="settings">
                <div class="form-group">
                    <label for="title">File name</label>
                    <input type="text" id="title" disabled class="form-control input-sm">
                </div>
                <div class="form-group">
                    <label for="slider_name">Slider name</label>
                    <input type="text" id="slider_name" disabled class="form-control input-sm">
                </div>
                <div class="form-group">
                    <label for="position">Bubble position</label>
                    <select id="position" class="form-control input-sm">
                        <option value="left">Left side</option>
                        <option value="right">Right side</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="rotation">Bubble rotation</label>
                    <select id="rotation" class="form-control input-sm">
                        <option value="top">Top</option>
                        <option value="bottom">Bottom</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="bubble_text" id="bt">Bubble text</label>
                    <textarea id="bubble_text" class="form-control" rows="3">

                    </textarea>
                </div>
                <div class="form-group">
                    <label for="show">Show</label>
                    <input type="checkbox" id="show">
                </div>
                <div class="form-group">
                    <label for="order">Order number</label>
                    <select id="order" class="form-control input-sm">

                    </select>
                </div>
                <button id="saveBanner" class="btn btn-small btn-success">Save</button>
            </div>
            <div class="form-group">
                <label for="file2">Upload new banner image</label>
                <div class="cover">
                    <div id="file2"><span class="glyphicon glyphicon-folder-open dz-clickable" aria-hidden="true"></span>Select file</div>
                    <div class="filename2"></div>
                    <button class="uploader2">Upload</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-1 col-md-10 employee-banner">
            <h4>Employee banner settings</h4>
            <h5>Already uploaded banners:</h5>
            <?php
            $slider3 = array_diff(scandir('../web/carousel/slider3/'), array('..', '.','.DS_Store'));
            foreach ($slider3 as $slide){
                echo '<div class="img-container3"><img class="slider-img" width=200 src="../carousel/slider3/'.$slide.'"><button class="remover" id="slider3/'.$slide.'">Remove</button></div>';
            }
            ?>
            <h5>Choose a banner to arrange settings</h5>
            <div class="settings">
                <div class="form-group">
                    <label for="title1">File name</label>
                    <input type="text" id="title1" disabled class="form-control input-sm">
                </div>
                <div class="form-group">
                    <label for="slider_name1">Slider name</label>
                    <input type="text" id="slider_name1" disabled class="form-control input-sm">
                </div>
                <div class="form-group">
                    <label for="position1">Bubble position</label>
                    <select id="position1" class="form-control input-sm">
                        <option value="left">Left side</option>
                        <option value="right">Right side</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="rotation1">Bubble rotation</label>
                    <select id="rotation1" class="form-control input-sm">
                        <option value="top">Top</option>
                        <option value="bottom">Bottom</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="bubble_text1" id="bt">Bubble text</label>
                    <textarea id="bubble_text1" class="form-control" rows="3">

                    </textarea>
                </div>
                <div class="form-group">
                    <label for="show1">Show</label>
                    <input type="checkbox" id="show1">
                </div>
                <div class="form-group">
                    <label for="order1">Order number</label>
                    <select id="order1" class="form-control input-sm">
                        <option></option>
                    </select>
                </div>
                <button id="saveBanner1" class="btn btn-small btn-success">Save</button>
            </div>

            <div class="form-group">
                <label for="file3">Upload new banner image</label>
                <div class="cover">
                    <div id="file3"><span class="glyphicon glyphicon-folder-open dz-clickable" aria-hidden="true"></span>Select file</div>
                    <div class="filename3"></div>
                    <button class="uploader3">Upload</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //Creating 3 dropzones for each slider
    var dropzone1 = new Dropzone("#file1", {
        url: '../upload_script.php',
        maxFiles: 1,
        parallelUploads: 1,
        uploadMultiple: false,
        autoQueue:true,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        headers: { "slide": 1 }
    });
    dropzone1.on("addedfile", function(file) {
        $('.filename1').text(file.name);
    });
    $('.uploader1').click(function(){
        $('.ttban').addClass('loading');
        dropzone1.processQueue();
    });
    dropzone1.on("success", function(file){
        $('.filename1').text("");
        $(".img-container1:last").after('<div class="img-container1"><img class="slider-img" width=200 src="../carousel/slider1/'+ file.name +'"><button class="remover" id="slider1/'+ file.name +'">Remove</button></div>')
        $('.ttban').removeClass('loading');
        $('.remover').bind("click", function(){
            $(this).parent().remove();
            $.ajax({
                type: 'post',
                data: {fileName : $(this).attr('id')}
            });
        });
    });

    var dropzone2 = new Dropzone("#file2", {
        url: '../upload_script.php',
        maxFiles: 1,
        parallelUploads: 1,
        uploadMultiple: false,
        autoQueue:true,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        headers: { "slide": 2 }
    });
    dropzone2.on("addedfile", function(file) {
        $('.filename2').text(file.name);
    });
    $('.uploader2').click(function(){
        $('.clients-banner').addClass('loading');
        dropzone2.processQueue();
    });
    dropzone2.on("success", function(file){
        $('.filename2').text("");
        $(".img-container2:last").after('<div class="img-container2"><img class="slider-img" width=200 src="../carousel/slider2/'+ file.name +'"><button class="remover" id="slider2/'+ file.name +'">Remove</button></div>')
        $('.clients-banner').removeClass('loading');
        $('.remover').bind("click", function(){
            $(this).parent().remove();
            $.ajax({
                type: 'post',
                data: {fileName : $(this).attr('id')},
            });
        });
    });

    var dropzone3 = new Dropzone("#file3", {
        url: '../upload_script.php',
        maxFiles: 1,
        parallelUploads: 1,
        uploadMultiple: false,
        autoQueue:true,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        headers: { "slide": 3 }
    });
    dropzone3.on("addedfile", function(file) {
        $('.filename3').text(file.name);
    });
    $('.uploader3').click(function(){
        $('.employee-banner').addClass('loading');
        dropzone3.processQueue();
    });
    dropzone3.on("success", function(file){
        $('.filename3').text("");
        $(".img-container3:last").after('<div class="img-container1"><img class="slider-img" width=200 src="../carousel/slider3/'+ file.name +'"><button class="remover" id="slider3/'+ file.name +'">Remove</button></div>')
        $('.employee-banner').removeClass('loading');
        $('.remover').bind("click", function(){
            $(this).parent().remove();
            $.ajax({
                type: 'post',
                data: {fileName : $(this).attr('id')}
            });
        });
    });

    //----------------------------------------------------------------------//

    $('.remover').bind("click", function(){
        $(this).parent().remove();
        var slideName = $(this).attr('id').split('/');
        $.ajax({
            type: 'post',
            data: {fileName : $(this).attr('id')},
            success: function(response){
            }
        });
    });

/*-------------------------------Slider 2 ---------------------------------------------*/
    /*--------*/

    /*----------*/
    $(document).on('click','.img-container2 > img',function(){
        $('img').removeClass('choosen');
        $(this).addClass('choosen');
        $('textarea').val('');
        var arr = $(this).attr('src').split('/');
        var title = arr[arr.length-1];
        var slider = 'slider2';

        $('.clients-banner #title').val(title);
        $('.clients-banner #slider_name').val(slider);
        $.ajax({
            url: '../slide_script.php',
            type: 'post',
            data: {
                type: 'get_data',
                title : title,
                slider : slider
            },
            success: function (response) {
                var obj = JSON.parse(response);
                console.log(response);
                $('#position').val(obj[0].bubble_position);
                $('#rotation').val(obj[0].bubble_rotation);
                $('#bubble_text').val(obj[0].bubble_text);
                if ((obj[0].showw) == 'true') {
                    $('#show').prop('checked', true);
                    $('#order').prop('disabled',false);
                }else {
                    $('#show').prop('checked', false);
                    $('#order').prop('disabled',true);
                }

                var quantity = obj['quantity'];
                $('#order').empty();
                $('#order').append('<option class="opt" value=""></option>');
                for (var i = 0; i < quantity; i++) {
                    $('#order').append('<option class="opt" value="'+(i+1)+'">'+(i+1)+'</option>');
                }
                $($(".opt[value='"+obj[0].order_number+"']")).attr('selected', 'selected');
            }
        });


    });
    $(document).on('click','#saveBanner',function(){
        var title = $('#title').val();
        var slider = $('#slider_name').val();
        var position = $('#position').val();
        var rotation = $('#rotation').val();
        var text = $('#bubble_text').val();
        var show = $('#show').is(":checked");
        var orderNumber = $('#order').val();
        $('.clients-banner').addClass('loading');
        $.ajax({
            url: '../slide_script.php',
            type: 'post',
            data: {
                type: 'insert_data',
                title : title,
                slider : slider,
                position : position,
                rotation : rotation,
                text : text,
                show : show,
                order : orderNumber
            },
            success: function (response) {
                $('.clients-banner').removeClass('loading');

            }
        });
    });
    /*-------------------------------------------------------------------------------------*/
    /*-------------------------------Slider 3 ---------------------------------------------*/
    $(document).on('click','.img-container3 > img',function(){
        $('img').removeClass('choosen');
        $(this).addClass('choosen');
        $('textarea').val('');
        var arr = $(this).attr('src').split('/');
        var title = arr[arr.length-1];
        var slider = 'slider3';
        $('.employee-banner #title1').val(title);
        $('.employee-banner #slider_name1').val(slider);
        $.ajax({
            url: '../slide_script.php',
            type: 'post',
            data: {
                type: 'get_data',
                title : title,
                slider : slider
            },
            success: function (response) {
                var obj = JSON.parse(response);
                console.log(response);
                $('#position1').val(obj[0].bubble_position);
                $('#rotation1').val(obj[0].bubble_rotation);
                $('#bubble_text1').val(obj[0].bubble_text);
                if ((obj[0].showw) == 'true') {
                    $('#show1').prop('checked', true);
                    $('#order1').prop('disabled',false);
                }else {
                    $('#show1').prop('checked', false);
                    $('#order1').prop('disabled',true);
                }

                var quantity = obj['quantity'];
                $('#order1').empty();
                $('#order1').append('<option class="opt1" value=""></option>');
                for (var i = 0; i < quantity; i++) {
                    $('#order1').append('<option class="opt1" value="'+(i+1)+'">'+(i+1)+'</option>');
                }
                $($(".opt1[value='"+obj[0].order_number+"']")).attr('selected', 'selected');

            }
        });


    });
    $(document).on('click','#saveBanner1',function(){
        var title = $('#title1').val();
        var slider = $('#slider_name1').val();
        var position = $('#position1').val();
        var rotation = $('#rotation1').val();
        var text = $('#bubble_text1').val();
        var show = $('#show1').is(":checked");
        var orderNumber = $('#order1').val();
        $('.employee-banner').addClass('loading');
        $.ajax({
            url: '../slide_script.php',
            type: 'post',
            data: {
                type: 'insert_data',
                title : title,
                slider : slider,
                position : position,
                rotation : rotation,
                text : text,
                show : show,
                order : orderNumber
            },
            success: function (response) {
                $('.employee-banner').removeClass('loading');

            }
        });
    });
    /*-------------------------------------------------------------------------------------*/
</script>
<?php
if($_POST['fileName']!="") {
    unlink("../web/carousel/" . $_POST['fileName']);
    $fileName = $_POST['fileName'];
    $title = explode('/',$fileName);
    (new Query)->createCommand()->delete('slide', ['title' => $title])->execute();
}
?>