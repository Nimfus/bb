<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */
/* @var $form ActiveForm */
?>
<head>
<style>
    .container-fluid {
        max-height: 100%;
        background-color: #D0D0D0;
    }
.wrap {
	background-color: #D0D0D0;
	padding-top: 5%;
	font-family: 'Helvetica';
	font-weight: 100;

}
.admin-login {
	background: white;
	padding-top: 30px;
	padding-right: 20px;
	padding-left: 20px;
	margin: auto;
	width: 300px;
	height: 290px;
	border: 1px solid #71BD45;
	border-radius: 8px;
}
    .ma {
        display: none;
    }
</style>
</head>
<div class="row wrap">
<div class="admin-login">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput(); ?>
    
        <div class="form-group">
            <?= Html::submitButton('Log In', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- admin-login -->
</div>