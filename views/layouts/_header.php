<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
<style>
body {
	font-family: 'Open Sans', arial, sans-serif;
}
.col-md-12,
.container {
    padding: 0;
}
/* Navigation menue settings */
#w0 {
	background: white;
	border-bottom: 1px solid #BCBCBC;
}
#w0 > .container{
		margin: 0;
		width: 100%;
}
.navbar-brand{
	margin-right: 30px;
	width: 135px;
	height: 100px;
	padding-bottom: 0;
	padding-right: 0;
	margin-bottom: 0;
	background-position: center;
	background-image: url('../web/images/logo.png');
	background-position-x: 80%;
}
.navigation{
	margin-bottom: 0;
	border: none;
}
.nav {
	min-height: 80px;
	vertical-align: center;
}
.nav > li {
	padding-top: 47px;
	width: 138px;
	text-align: center;
}
.nav-buttons > li > a {
	color: #BCBCBC;
	font-size: 17px;
	font-weight: 200;
	padding-top: 0px;
	padding-bottom: 30px;
	font-weight: 100;
}
.nav-buttons > li {
	border-bottom: 3px solid white;
}
.nav-buttons > li > a:hover {
	background-color: #FFFFFF;
	color: #71BD45;
    transition: 0.3s;
}	
.nav-buttons > li:hover {
	background: none;
	background-color: #FFFFFF;
	border-bottom: 3px solid #71BD45;
    transition: 0.3s;
}
.container-fluid {
	padding-right: 0px;
}
li.selected {
	background: none;
	background-color: #FFFFFF;
	border-bottom: 3px solid #71BD45;
	color: #71BD45;
}
li.selected > a {
	color: #71BD45;
}
/*---------------------------------*/
/*Main-slide settings*/
.main-slide {
	margin-top: 100px;
	background-image: url('../web/images/main-slide.jpg');
	background-position-x: 38%;
	background-position-y: 1%; 
	min-height: 460px;
	max-height: 470px;
	text-align: center;
}
.main-slide-text {
	font-size: 16px;
	width: 34%;
	max-width: 470px;
	margin: 150px auto;
	margin-left: 34%;
	margin-right: 33%;
}
.main-slide-text > h1 {
	font-size: 35px;
	font-weight: 400;
}
.main-slide-text > span {
	color:#71BD45;
}
.main-slide-text > p {
	padding-top: 10px;
	color: #BCBCBC;
	font-size: 18px;
	font-weight: 200;
}
.header-button {
	font-family: 'Helvetica';
    padding-top: 13px;
	margin-top: 20px;
	width: 240px;
	height: 50px;
	color: white;
	font-weight: 200;
	font-size: 16px;
	background-color: #71BD45;
}
.header-button:hover {
	/* Fill here button text color */
}
.carousel-indicators .active  {
    background-color: #71BD45;
}

/*---------------------------------*/
/* Soluton slide */
.solutions-slide-header {
	padding-top: 50px;
	text-align: center;
	font-size: 16px;
	width: 34%;
	max-width: 470px;
	margin: 20px auto;
	margin-left: 34%;
	margin-right: 33%;
}
.solutions-slide-header > h1 {
	font-size: 35px;
	font-weight: 400;
	margin-bottom: 20px;
}
.solutions-slide-header > h1 > span {
	color: #71BD45;
}
.solutions > p {
	margin-bottom: 1px;
}
.solutions-slide-text {
	text-align: center;
	font-size: 18px;
	width: 50%;
	max-width: 800px;
	margin: 20px auto;
	margin-left: 25%;
	margin-right: 25%;
	margin-bottom: 20px;
}
/*Images in the grid*/
.img-grid {
	height: 230px;
	width: 100%;
}
.img-grid-insider {
	height: 230px;
	width: 960px;
	margin: 0 auto;
}
.solutions {
	padding-right: 0;
	padding-left: 0;
	height: 230px;
	width: 240px;
	background-size: 100%;
	background-position-x: 50%;
	background-repeat: none;
	text-align: center;
	padding-top: 140px;
	cursor: pointer;
	margin-bottom: 40px;
    transition: 0.2s;

}
.creation {
	background-image: url('../web/images/creation-bg.png');
}
.mobile {
	background-image: url('../web/images/mobile-bg.png');
}
.customer {
	background-image: url('../web/images/customer-bg.png');
}
.employee {
	background-image: url('../web/images/employee-bg.png');
}
.productivity {
	background-position-x: -8px;
	background-image: url('../web/images/productivity-bg.png');
}
.marketing {
	background-image: url('../web/images/marketing-bg.png');
}
.design {
	background-image: url('../web/images/design-bg.png');
}
.condo {
	background-image: url('../web/images/condo-bg.png');
}
/*Solutions reaction*/
.reaction-header {
	color: white;
	font-size: 16px;
}
.reaction-list {
	font-family: 'Helvetica';
	padding-top:8px;
	text-align: left;
	color: white;
	font-size: 15px;
	font-weight: 100;
	margin-left: 5px;
	padding-right: 5px;
}
/*---/
/*---------------------------------*/
/*Sliders 1,2 and 3 */
.slider1,
.slider2,
.slider3 {
	text-align: center;
	height: 100%;
}
.carousel-control.left,
.carousel-control.right {
	font-family: 'Helvetica';
	font-size: 28px;
	font-weight: 100;
	text-align: center;
	border-radius: 25px;
	height: 50px;
	width: 50px;
	background-image: none;
	background-color: #FFFFFF;
	color: #BCBCBC;
	text-shadow: none;
	opacity: 1;
	position: absolute;
	top: 80%;
	bottom: 100px;
	right: 10%;
	padding-top: 3px;
	padding-left: 4px;
}
.carousel-control.left {
	left: 82%;
	right: 240px;
	padding-right: 5px;
}
#slider2 > .carousel-control.left,
#slider2 > .carousel-control.right,
#slider3 > .carousel-control.left,
#slider3 > .carousel-control.right {
	background-color: #BCBCBC;
	color: white;
	opacity: 0.8;
	top: 50%;
}
#slider2 > .carousel-control.left,
#slider3 > .carousel-control.left {
	left: 5%;
}
#slider2 > .carousel-control.right,
#slider3 > .carousel-control.right {
	right: 5%;
}
.carousel-control.left:hover,
.carousel-control.right:hover {
	background-color: #71BD45;
	color: white;
}
#slider2 > .carousel-control.left:hover,
#slider2 > .carousel-control.right:hover,
#slider3 > .carousel-control.left:hover,
#slider3 > .carousel-control.right:hover {
	background-color: #71BD45;
	color: white;
	opacity: 0.9;
}
/*---------------------------------*/
/* Portfolio grid */

.portfolio-grid {
	padding-top: 40px;
	height: 250px;
	width: 100%;
	margin-bottom: 60px;
}
.portfolio-grid-insider {
	height: 250px;
	width: 1200px;
	margin: 0 auto;
}
.portfolio {
	width: 300px;
    transition: 0.3s;
}
.portfolio img {
	cursor: pointer;
}
/*---------------------------------*/
/*Employees*/
.join-button {
	font-family: 'Helvetica';
	position: absolute;
	bottom: 150px;
	right: 15%;
	width: 130px;
	height: 50px;
	color: white;
	font-weight: 100;
	font-size: 20px;
	background-color: #71BD45;
}
/*---------------------------------*/
/*Contact Us*/
.contact-left {
	font-family: 'Helvetica';
	font-size: 14px;
	font-weight: 100;
	margin-top: 60px;
	padding-left: 10%;
	text-align: left;
	margin-bottom: 10px;
}
.contact-right {
	margin-top: 60px;
	padding-right: 9%;
	margin-bottom: 10px;
}
.form-group > label {
	padding-right: 5px;
	font-size: 16px;
	font-weight: 200;
}
.form-group > label > span {
	color:#71BD45;
}
.first {
	font-family: 'Helvetica';
	font-size: 23px;
	font-weight: 100;
	font-size: 16px;
	font-weight: 100;
	max-width: 250px;
}
input {
	margin-left: -40px;
	width: 90%;
	border: 1px solid #D0D0D0;
}
#message {
	margin-left: -40px;
	width: 90%;
	max-width: 90%;
	max-height: 200px;
	height: 200px;
	border: 1px solid #D0D0D0;
	resize: none;
}
.contact-button {
	font-family: 'Helvetica';
	margin-top: 20px;
	margin-left: -30px;
	width: 100px;
	height: 50px;
	color: white;
	font-weight: 200;
	font-size: 16px;
	background-color: #71BD45;
}
.contact-error {
	color: red;
	font-weight: 200;
	padding-left: 10px;
}
/*---------------------------------*/
/*Footer*/
.footer {
    padding-top: 17px;
	margin-top: 40px;
	background-color: white;
	border-top: 1px solid #D0D0D0;
	height: 120px;
}
.footer-logo {
	padding-top: 0px;
	padding-left: 50px;
}
.footer-social {
	padding-top: 2px;
	padding-left: 150px;
}
.footer-social a {
	padding-right: 7px;
}

/*---------------------------------*/
#map {
	margin-top: 10px;
	width: 450px;
	height: 200px;
}
.anchor {
	display: hidden;
	min-height: 150px;
	padding-bottom: 100px;
}
.carousel-indicators > li {
	border: 2px solid white;
}
    *:hover {
        transition: 0.3s;
    }
</style>

</head>
<body>
<div class="container-fluid">
	<?php
    NavBar::begin([
        'brandLabel' => '',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-fixed-top navigation',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left nav-buttons'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Solutions', 'url' => ['/site/index','#'=>'solutions']],
            ['label' => 'Promotion', 'url' => ['/site/promotion']],
            ['label' => 'Clients', 'url' => ['/site/index','#'=>'clients']],
            ['label' => 'Portfolio', 'url' => ['/site/portfolio']],
            ['label' => 'Blogs', 'url' => ['/site/blog']],
            ['label' => 'Employees', 'url' => ['/site/index','#'=>'employee']],
            ['label' => 'Contact', 'url' => ['/site/index','#'=>'contactus']],
        ],
    ]);
    NavBar::end();
    ?>
</div>
</body>
</html>