<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 footer-logo">
            <a href=""><img src="../web/images/footer-left.png"></a>
        </div>
        <div class="col-md-4 footer-social">
           <a href="https://www.facebook.com/businessbuddysg"><img src="../web/images/fb.png"></a> 
           <a href="https://twitter.com/businessbuddysg"><img src="../web/images/tw.png"></a>
           <a href="https://plus.google.com/+BusinessbuddySg/about"><img src="../web/images/google.png"></a>
           <a href="https://www.pinterest.com/businessbuddysg/"><img src="../web/images/p.png"></a>
           <a href="https://www.linkedin.com/company/businessbuddy"><img src="../web/images/in.png"></a>
        </div>
    </div>
</div>
<script>
function initMap() { //Google maps 
  var myLatLng = {lat: 1.275303, lng: 103.846333};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Business Buddy Pte Ltd'
  });
} //----------

</script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap">
</script>
