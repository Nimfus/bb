<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>
<style>
    .admin-header {
        min-height: 50px;
        max-height: 50px;
    }
    #w0 .container {
        margin-left: 30px;
    }
    .wrap {
        min-height: 100px;
        padding-left: 0;
        padding-right: 0;
    }
    .dashboard-img {
        height: 50px;
        width: 50px;
        margin-right: 20px;
        margin-bottom: 10px;
    }
    .admin-navigation {
        width: 100%;
        background: #B2F0B2;
    }
    .container {
        margin-left: 10px;
    }
    .container-fluid {
        padding-left: 0;
        padding-right: 0;
        min-height: 100%;
    }

</style>
<div class="container-fluid ma">
    <div class="row">
        <div class="col-md-12 admin-header">
            <div class="col-md-4">
                <h4><img class="dashboard-img" src=" http://localhost/businessbuddy/web/images/dashboard-icon.png">Business Buddy Backend</h4>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12 fl">
            <?php
            NavBar::begin([
                'brandLabel' => '',
                'options' => [
                    'class' => 'navbar-static-top admin-navigation',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left nav-buttons'],
                'items' => [
                    ['label' => 'Landing Page', 'url' => ['admin/index']],
                    ['label' => 'Promotion', 'url' => ['admin/promotion']],
                    ['label' => 'Portfolio', 'url' => ['admin/portfolio']],
                    ['label' => 'Blog', 'url' => ['admin/blog']],
                    ['label' => 'Get started', 'url' => ['admin/get-started']],
                    ['label' => 'We are hiring', 'url' => ['admin/vacancy']],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Login', 'url' => ['/admin/login']] :
                        [
                            'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['admin/logout'],
                            'linkOptions' => ['data-method' => 'post'],
                        ],
                ],
            ]);
            NavBar::end();
            ?>
        </div>
    </div>
</div>
</div>