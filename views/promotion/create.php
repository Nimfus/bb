<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Models\Promotion */

$this->title = 'Create Promotion';
$this->params['breadcrumbs'][] = ['label' => 'Promotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .promotion-create {
        padding-right: 10%;
        padding-left: 10%;
    }
</style>
<div class="promotion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
