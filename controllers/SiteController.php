<?php

namespace app\controllers;

use app\models\ContactusForm;
use Yii;
use yii\web\Controller;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use \yii\data\Pagination;


class SiteController extends Controller
{


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionBlog()
    {
        $query = \app\models\Post::find()->addOrderBy('time DESC');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize'=>3]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('blog', [
            'models' => $models,
            'pages' => $pages,
        ]);
        //return $this->render('blog');
    }

    public function actionPortfolio()
    {
        return $this->render('portfolio');
    }
    public function actionGetStarted()
    {
        return $this->render('started');
    }

    public function actionPromotion()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Promotion::find(),
        ]);

        return $this->render('promotion', [
            'dataProvider' => $dataProvider,
        ]);
        //return $this->render('promotion');
    }
    public function actionHiring()
    {
        return $this->render('hiring');
    }
    public function actionContactus()
    {
        $model = new ContactusForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // valid data received in $model

            $model->contact('nimfus@gmail.com');

            return 1;
        } else {
            // either the page is initially displayed or there is some validation error
            return 2;
        }
    }

    protected function findModel($id)
    {
        if (($model = \app\models\Promotion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
