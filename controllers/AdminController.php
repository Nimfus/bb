<?php
namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class AdminController extends \yii\web\Controller
{
	public $layout = 'backend';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
	{
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            
            return $this->redirect('admin/index');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionPortfolio()
    {
        return $this->render('portfolio');
    }

    public function actionPromotion()
    {
        return $this->render('promotion');
    }

    public function actionHiring()
    {
        return $this->render('vacancy');
    }

    public function actionGetStarted()
    {
        return $this->render('getStarted');
    }

}
