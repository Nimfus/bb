<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Coldmans1',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '' => 'site/index',
                'admin' => 'admin/login',
                'promotion' => 'site/promotion',
                'portfolio' => 'site/portfolio',
                'blog' => 'site/blog',
                'hiring' => 'site/hiring',
                'started' => 'site/get-started',
                'admin/index' => 'admin/index',
                'admin/promotion' => 'promotion/index',
                'admin/portfolio' => 'portfolio/index',
                'admin/blog' => 'post/index',
                'admin/vacancy' => 'vacancy/index',
                'admin/get-started' => 'product/index',
                'promotion/create' => 'promotion/create',
                'promotion/view' => 'promotion/view',
                'promotion/update' => 'promotion/update',
                'promotion/delete' => 'promotion/delete',
                'portfolio/create' => 'portfolio/create',
                'portfolio/view' => 'portfolio/view',
                'portfolio/update' => 'portfolio/update',
                'portfolio/delete' => 'portfolio/delete',
                'post/create' => 'post/create',
                'post/view' => 'post/view',
                'post/update' => 'post/update',
                'post/delete' => 'post/delete',
                'vacancy/create' => 'vacancy/create',
                'vacancy/view' => 'vacancy/view',
                'vacancy/update' => 'vacancy/update',
                'vacancy/delete' => 'vacancy/delete',
                'product/create' => 'product/create',
                'product/view' => 'product/view',
                'product/update' => 'product/update',
                'product/delete' => 'product/delete',
                'admin/tag' => 'tag/index',
                'tag/create' => 'tag/create',
                'tag/view' => 'tag/view',
                'tag/update' => 'tag/update',
                'tag/delete' => 'tag/delete',
                'site/login' => 'admin/login',
                'admin/logout' => 'admin/logout',
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
